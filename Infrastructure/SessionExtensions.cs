/*
    The session state feature in ASP.NET Cores stores only int, string, and byte[] values.
    To store an object, we need to define extension methods to the ISession interface; which provide access to the
    session state data to serialize object into JSON and convert them back
    more info at : https://benjii.me/2016/07/using-sessions-and-httpcontext-in-aspnetcore-and-mvc-core/
 */
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace SportsStore.Infrastructure {
    public static class SessionExtensions {
        public static void SetJson(this ISession session, string key, object value) 
        {
            session.SetString(key, JsonConvert.SerializeObject(value));
        }
        public static T GetJson<T>(this ISession session, string key) 
        {
            var sessionData = session.GetString(key);
            return sessionData == null ? default(T) : JsonConvert.DeserializeObject<T>(sessionData);
        }
    }
}