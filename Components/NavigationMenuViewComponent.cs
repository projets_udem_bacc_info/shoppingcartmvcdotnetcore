// this class helps to create a system of reusable navigation control and 
// it is going to be integrated in the shared layout
// it helps creates smaller segments of an application while preserving the overall MVC approach
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using SportsStore.Models;

namespace SportsStore.Components {

    public class NavigationMenuViewComponent : ViewComponent {

        private IProductRepository repository; // helps getting the liste of product which will be category filtered
        // cstor
        public NavigationMenuViewComponent(IProductRepository repo)
        {
            repository = repo;
        }
        // the invoke methode is called when the component is used in a Razor view and the result of it is 
        // inserted into the HTML sent to the browser
        // within a view, view components are used through the @awaitComponent.InvokeAsynch expression
        // RouteData property from the ViewComponent Class helps to access the request data in order to get the value for
        // the current selected category
        public IViewComponentResult Invoke() {
            ViewBag.SelectedCategory = RouteData?.Values["category"]; // to pass the category to the view :could use a view model class for that
            return View(repository.Products
                .Select(x => x.Category)
                .Distinct()
                .OrderBy(x =>x));
        }
    }
}