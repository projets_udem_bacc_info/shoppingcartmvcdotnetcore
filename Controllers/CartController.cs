using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SportsStore.Infrastructure;
using SportsStore.Models;
using SportsStore.Models.ViewModels;

namespace SportsStore.Controllers {
    public class CartController : Controller {
        private IProductRepository repository;
        private Cart cart;
        public CartController(IProductRepository repo, Cart cartService) {
            repository = repo;
            cart = cartService;
        }
        // index method of cart page
        public ViewResult Index(string returnUrl)
        {
            return View(new CartIndexViewModel {
                            Cart = this.cart,
                            ReturnUrl = returnUrl
                        });
        }
        // RedirectToAction method send an HTTP redirect instruction to the client browser asking browser
        // to request a new URL : In this case Index action of the cart controller.
        public RedirectToActionResult AddToCart(int productId, string returnUrl) 
        {
            // on recoit le paramètre envoyé par post du formulaire, ensuite on vérifie si le produit fait partie
            // de la base de données ou existe tout simplement, et on l'ajoute au shopping cart
            Product product = repository.Products
                .FirstOrDefault(p => p.ProductID == productId);
            if (product != null) 
            {
                cart.AddItem(product, 1);
            }
            return RedirectToAction("Index", new { returnUrl }); // return where we left off while shopping
        }
        public RedirectToActionResult RemoveFromCart(int productId, string returnUrl) 
        {
            Product product = repository.Products
                .FirstOrDefault(p => p.ProductID == productId);
            if (product != null) {
                cart.RemoveLine(product);
            }
            return RedirectToAction("Index", new { returnUrl });
        }
        // utilisation de ASP.NET session state feature
        // private Cart GetCart() 
        // {// si la session est présente et que le cart existe pas, on renvoit une nouveau cart
        // // ?? called null coalescing operator : set Session<cart> to new Cart() if Session<Cart> is undefined or not present
        //     Cart cart = HttpContext.Session.GetJson<Cart>("Cart") ?? new Cart();
        //     return cart;
        // }
        // private void SaveCart(Cart cart) 
        // {
        //     HttpContext.Session.SetJson("Cart", cart);
        // }
    }
}