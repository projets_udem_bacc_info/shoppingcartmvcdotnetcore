/*
    this class helps us to implement services that requires to process an order,
    Actions methods to handle HTTP form POST request when user want to complete order
 */
using Microsoft.AspNetCore.Mvc;
using SportsStore.Models;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace SportsStore.Controllers {
    public class OrderController : Controller {
        private IOrderRepository repository;
        private Cart cart;
        // cstor
        public OrderController(IOrderRepository repoService, Cart cartService) {
            repository = repoService;
            cart = cartService;
        }
        /*
            The List method selects all the Order objects in the repository that have a Shipped value of false and
            passes them to the default view. This is the action method that we will use to display a list of the unshipped
            orders to the administrator
         */
        [Authorize]
        public ViewResult List() => View(repository.Orders.Where(o => !o.Shipped));
        /*
            The MarkShipped method will receive a POST request that specifies the ID of an order, which is used to
            locate the corresponding Order object from the repository so that the Shipped property can be set to true
            and saved
         */
        [HttpPost]
        [Authorize]
        public IActionResult MarkShipped(int orderID) {
            Order order = repository.Orders
                .FirstOrDefault(o => o.OrderID == orderID);
            if (order != null) {
                order.Shipped = true;
                repository.SaveOrder(order);
            }
            return RedirectToAction(nameof(List));
        }
        // The checkout method returns the default view and passes a new shippingDetails object as the view model
        // View result is when we land on the order page before submiting the order
        public ViewResult Checkout() => View(new Order());
        /*
            The Checkout action method is decorated with the [HttpPost] attribute, which means that it will be
            invoked for a POST request—in this case, when the user submits the form. Once again, we are relying on the
            model binding system so that We can receive the Order object, which we then complete using data from the Cart
            and store in the repository.
            MVC checks the validation constraints that we applied to the Order class using the data annotation
            attributes, and any validation problems are passed to the action method through the ModelState property.
            we can see whether there are any problems by checking the ModelState.IsValid property. we call the
            ModelState.AddModelError method to register an error message if there are no items in the cart
            IActionResult Checkout() is called when the form of order is submitted http post or get
         */
        [HttpPost]
        public IActionResult Checkout(Order order) {
            if (cart.Lines.Count() == 0) {
                ModelState.AddModelError("", "Sorry, your cart is empty!");
            }
            if (ModelState.IsValid) {
                order.Lines = cart.Lines.ToArray();
                repository.SaveOrder(order);
                return RedirectToAction(nameof(Completed));
            } else {
                return View(order);
            }
        }
        public ViewResult Completed() {
            cart.Clear();
            return View();
        }
    }
}