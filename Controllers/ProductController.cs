using Microsoft.AspNetCore.Mvc;
using SportsStore.Models;
using System.Linq;
using SportsStore.Models.ViewModels;

namespace SportsStore.Controllers {
    public class ProductController : Controller {

        private IProductRepository repository;
        public int PageSize = 4;
        /*
        dependency injection : allowing the product controller cstor to access the application's rep through the 
        IProduct repository interface without having any need to know which implementation class has been configured
        grace au service de configuration fait dans startup class
        Plus tard le repository serait remplacer sans que le controller le sache // chap 18 :
         */
        public ProductController(IProductRepository repo) {
            repository = repo;
        }
        // liste pour voir les produits rendus à la classe controller
        // => veut dire qu'on retourne la liste des produits à la vue venant du repository(dépost d'objet de la bdd)
        // ici on specifie pas le view, mvc va prendre celui par défaut
        // on va ajouter la categorie au filtre rendu par la page grace à l'ajout fait dans productListViewModel
        public ViewResult List(string category, int productPage = 1)
            => View(new ProductsListViewModel {
                Products = repository.Products
                    .Where(p => category == null || p.Category == category)
                    .OrderBy(p => p.ProductID)
                    .Skip((productPage - 1) * PageSize)
                    .Take(PageSize),
                PagingInfo = new PagingInfo {
                    CurrentPage = productPage,
                    ItemsPerPage = PageSize,
                    TotalItems = category == null ?
                        repository.Products.Count() :
                        repository.Products.Where(e =>
                            e.Category == category).Count()
                },
                CurrentCategory = category
            });
    }
}