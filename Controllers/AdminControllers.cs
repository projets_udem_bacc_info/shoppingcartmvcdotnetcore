using Microsoft.AspNetCore.Mvc;
using SportsStore.Models;
using System.Linq;
using Microsoft.AspNetCore.Authorization;


namespace SportsStore.Controllers {

    [Authorize]
    public class AdminController : Controller {
        private IProductRepository repository;
        //The controller constructor declares a dependency on the IProductRepository interface, which will be
        //resolved when instances are created
        public AdminController(IProductRepository repo) {
            repository = repo;
        }
        //The method Index, that calls the View
        // method to select the default view for the action, passing the set of products in the database as the view model
        public ViewResult Index() => View(repository.Products);
        // this method finds the product with the ID that corresponds to the producID parameter and passes it as a view
        // model object to the View
        public ViewResult Edit(int productId) =>
            View(repository.Products
                .FirstOrDefault(p => p.ProductID == productId));
        /* Handling POST request
            We check that the model binding process has been able to validate the data submitted by the user
            by reading the value of the ModelState.IsValid property. If everything is OK, we save the changes to the
            repository and redirect the user to the Index action so they see the modified list of products. If there is a
            problem with the data, we render the default view again so that the user can make corrections.

            After to have saved the changes in the repository, we store a message using the temp data feature, which is part
            of the ASP.NET Core session state feature. This is a key/value dictionary similar to the session data and view bag
            features used previously. The key difference from session data is that temp data persists until it is read.
            I cannot use ViewBag in this situation because ViewBag passes data between the controller and view
            and it cannot hold data for longer than the current HTTP request. When an edit succeeds, the browser
            is redirected to a new URL, so the ViewBag data is lost.
        */
        [HttpPost]
        public IActionResult Edit(Product product) 
        {
            if (ModelState.IsValid) {
                repository.SaveProduct(product);
                TempData["message"] = $"{product.Name} has been saved";
                return RedirectToAction("Index");
            } else {
                // there is something wrong with the data values
                return View(product);
            }
        }
        /*
            The Create action method, which is the one specified by the Add Product link in the
            main product list page. This will allow the administrator to add new items to the product catalog.
            The Create method does not render its default view. Instead, it specifies that the Edit view should be used.
            It is perfectly acceptable for one action method to use a view that is usually associated with another view. In this
            case, we provide a new Product object as the view model so that the Edit view is populated with empty fields.
         */
        public ViewResult Create() => View("Edit", new Product());

        [HttpPost]
        public IActionResult Delete(int productId) {
            Product deletedProduct = repository.DeleteProduct(productId);
            if (deletedProduct != null) {
                TempData["message"] = $"{deletedProduct.Name} was deleted";
            }
            return RedirectToAction("Index");
        }
        
    }
}