using System.Linq;

// interface permettant de stocker les information sur la base de données( tables produits)
// la classe qui dépendra de cette interface, ne saura pas les détails sur comment les produit sont stockés
// ou comment la classe va les livrer
namespace SportsStore.Models {
    public interface IProductRepository {
        // L'interface IQueryable permet à l'instance qui l'appelle l'obtention des objets produits en forme de sequence
        // il dérive de l'interface IEnumerable qui reprénsente la collection d'objets comme celle qui peuvent
        // être obtenus(queried) d'une base de données
        IQueryable<Product> Products { get; }
        // make the interface able to save changes made to products
        void SaveProduct(Product product);
        // method to delete product
        Product DeleteProduct(int productID);
    }
}