using System.Collections.Generic;
using SportsStore.Models;

namespace SportsStore.Models.ViewModels {
    public class ProductsListViewModel {
        public IEnumerable<Product> Products { get; set; }
        public PagingInfo PagingInfo { get; set; } // getting info about the paging
        // this will help highlighting the current category and linking to others.
        // it will also help to communicate the current category to the view in order to render the sidebar.
        public string CurrentCategory { get; set; }
    }
}