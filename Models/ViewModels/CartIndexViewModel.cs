using SportsStore.Models;
// utilisé souvent lorsque une controller fait appelle à un autre et que l'info appelé fait parti de l'affichage
// sur notre vue.
// une sorte de combinaison de deux models sur un meme controller
namespace SportsStore.Models.ViewModels {
    public class CartIndexViewModel {
        public Cart Cart { get; set; }
        public string ReturnUrl { get; set; }
    }
}