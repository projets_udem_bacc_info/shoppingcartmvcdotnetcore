/*
    Class to represent user's credentials

    The Name and Password properties have been decorated with the Required attribute, which uses model
    validation to ensure that values have been provided. The Password property has been decorated with the
    UIHint attribute so that when we use the asp-for attribute on the input element in the login Razor view, the
    tag helper will set the type attribute to password; that way, the text entered by the user isn’t visible on-
    screen

 */
using System.ComponentModel.DataAnnotations;

namespace SportsStore.Models.ViewModels {
    public class LoginModel {
        [Required]
        public string Name { get; set; }
        [Required]
        [UIHint("password")]
        public string Password { get; set; }
        public string ReturnUrl { get; set; } = "/";
    }
}