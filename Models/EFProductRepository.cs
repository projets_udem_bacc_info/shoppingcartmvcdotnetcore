/*
    this class uses the interface to manipulate object from and to the database
    create a database migration :
    - dotnet ef migrations add Initial
 */
using System.Collections.Generic;
using System.Linq;

namespace SportsStore.Models {
    public class EFProductRepository : IProductRepository {
        private ApplicationDbContext context;
        public EFProductRepository(ApplicationDbContext ctx) {
            context = ctx;
        }
        public IQueryable<Product> Products => context.Products;
        /*  implementation of the saveChanges method adds a product to the repository if the ProductID is 0;
            otherwise, it applies any changes to the existing entry in the database
            We perform an update when the product Id is not equal to zero
        */
        public void SaveProduct(Product product)
        {
            if (product.ProductID == 0) {
                context.Products.Add(product);
            } else {
                /*
                    Here Entity Framework Core keeps track of the objects that it creates from the database.
                    The object passed to the SaveChanges method is created by the MVC model binding feature, which means
                    that Entity Framework Core does not know anything about the new Product object and will not apply an
                    update to the database when it is modified. One of ways of resolving this issue, is to locate the corresponding 
                    object that Entity Framework Core does know about and update it explicitly.
                 */
                Product dbEntry = context.Products // getting a product from the repository to verify with productid from form
                    .FirstOrDefault(p => p.ProductID == product.ProductID);
                if (dbEntry != null) {
                    dbEntry.Name = product.Name;
                    dbEntry.Description = product.Description;
                    dbEntry.Price = product.Price;
                    dbEntry.Category = product.Category;
                }
            }
            context.SaveChanges();
        }
        //Deletion Support in the EFProductRepository
        public Product DeleteProduct(int productID) {
            Product dbEntry = context.Products
                .FirstOrDefault(p => p.ProductID == productID);
            if (dbEntry != null) {
                context.Products.Remove(dbEntry);
                context.SaveChanges();
            }
            return dbEntry;
        }
    }
}