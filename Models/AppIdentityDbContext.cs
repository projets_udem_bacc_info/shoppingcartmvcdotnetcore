/*
    A database context file that will act as the bridge between the database and the Identity
    model objects it provides access to
    After configuring the services in the startup class, use these command

    dotnet ef migrations add Initial --context AppIdentityDbContext

    dotnet ef database update --context AppIdentityDbContext

 */
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace SportsStore.Models {
    public class AppIdentityDbContext : IdentityDbContext<IdentityUser> {
        public AppIdentityDbContext(DbContextOptions<AppIdentityDbContext> options)
            : base(options) { }
    }
}