/*
    This class implements IOrderRepository using Entity Framework Core, allowing the set of Order
    objects that have been stored to be retrieved and allowing orders to be created or changed.
 */
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace SportsStore.Models {
    public class EFOrderRepository : IOrderRepository {
        private ApplicationDbContext context;
        public EFOrderRepository(ApplicationDbContext ctx) {
            context = ctx;
        }
        /*
            Entity Framework Core requires instruction to load related data if it spans multiple tables.
            in the Order Property we use Include and ThenInclude methods to specify that when an Order Object is read from the
            database, the collection associated with Lines property should also be loaded along with each Product Object
            associated with each collection object.
            This ensures that we receive all the data objects that we need without having to perform the queries and assemble
            the data directly
         */
        public IQueryable<Order> Orders => context.Orders
                            .Include(o => o.Lines)
                            .ThenInclude(l => l.Product);
        /*
            An additional step is required when we store an Order object in the database. When the user’s cart data
            is deserialized from the session store, the JSON package creates new objects that are not known to
            Entity Framework Core, which then tries to write all the objects into the database. For the Product
            objects, this means that Entity Framework Core tries to write objects that have already been stored,
            which causes an error. To avoid this problem, we notify Entity Framework Core that the objects exist and
            shouldn’t be stored in the database unless they are modified, by using context.AttachRange() method.
            This ensures that Entity Framework Core won’t try to write the deserialized Product objects that are
            associated with the Order object.
         */
        public void SaveOrder(Order order) {
            context.AttachRange(order.Lines.Select(l => l.Product));
            if (order.OrderID == 0) {
                context.Orders.Add(order);
            }
            context.SaveChanges();
        }
    }
}
