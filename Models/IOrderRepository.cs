/*
    Creating the order repository Interface to provide access to the order object from the database
 */
using System.Linq;

namespace SportsStore.Models {
    public interface IOrderRepository {
        IQueryable<Order> Orders { get; }
        void SaveOrder(Order order);
    }
}